import { PureComponent, Suspense } from "react";
import { connect } from "react-redux";
import Loading from "components/Loading/Loading";
import Modal from "components/Modal/Modal";
import { deleteProduct, setProducts } from "redux/actions";
import { api } from "helpers/axios";
import { Product } from "redux/slices/productSlice";
import { RootState } from "redux/store/store";
import ProductsList from "./ProductsList";
import { ToastContext } from "contexts/ToastContext";

type PropsType = {
    products: Product[];
    setProducts: (products: Product[]) => void;
    deleteProduct: (id: number) => void;
};

type StateType = {
    skip: number;
    limit: number;
    page: number;
    showModal: boolean;
    productId: number;
    error: string;
};

export class ProductsClass extends PureComponent<PropsType> {
    constructor(props: PropsType) {
        super(props);
    }

    state: StateType = {
        skip: 0,
        limit: 10,
        page: 1,
        showModal: false,
        productId: 0,
        error: "",
    };
    static contextType = ToastContext;

    onAcceptDelete = async () => {
        try {
            await api.delete(`/products/${this.state.productId}`);
            this.props.deleteProduct(this.state.productId);
            const { addToast }: any = this.context;
            addToast("Successfully deleted!", "success");
            this.setState((prev) => ({ ...prev, showModal: false, productId: 0 }));
        } catch (err: any) {
            this.setState((prev) => ({ ...prev, error: err.message }));
        }
    };
    onCloseModal = () => {
        this.setState((prev) => ({ ...prev, showModal: false }));
    };
    // onPrevious() {}
    // onNext() {}
    render() {
        return (
            <section className="home-product mt-4">
                <div className="container">
                    <div className="row">
                        <div className="col-12 position-relative">
                            {this.state.showModal && (
                                <Modal onAccept={this.onAcceptDelete} onCloseModal={this.onCloseModal}>
                                    <h4>Do you want to delete this product?</h4>
                                </Modal>
                            )}
                            <h2 className="text-center title">
                                <span>Products</span>
                            </h2>
                            <Suspense fallback={<Loading />}>
                                <ProductsList setError={(e) => this.setState((prev) => ({ ...prev, error: e }))} setShowModal={(e) => this.setState((prev) => ({ ...prev, showModal: e }))} setProductId={(e) => this.setState((prev) => ({ ...prev, productId: e }))} />
                            </Suspense>
                            {/* <div className="home-product__navigator">
                                <Button
                                    className="btn-prev position-relative"
                                    // onClick={this.onPrevious}
                                ></Button>
                                <Button
                                    className="btn-next position-relative"
                                    // onClick={this.onNext}
                                ></Button>
                            </div> */}
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

const mapStateToProps = (state: RootState) => ({
    products: state.product.products,
});

const mapDispatchToProps = { setProducts, deleteProduct };

export default connect(mapStateToProps, mapDispatchToProps)(ProductsClass);
